var dummy_data_category_count = -1;
var dummy_data_num = 10;
jQuery(document).ready(function() {
	jQuery('#dummy_data_ajax_insert_post ' + 'button').click(function(event) {
		event.preventDefault();
		jQuery(this).attr('disabled', 'disabled').text('טוען');
		if (dummy_data_category_count != -1) {

		} else {
			dummy_data_category_count = dummy_data_categories.length;
			dummy_data_insert_post(
				jQuery('#dummy_data_ajax_insert_post ' + 'input[name=dummy_data_num_posts]').val(),
				(jQuery('#dummy_data_ajax_insert_post ' + 'input[name=dummy_data_leaf_only]').attr('checked') == 'checked') ? '1' : '0',
				(jQuery('#dummy_data_ajax_insert_post ' + 'input[name=dummy_data_random_author]').attr('checked') == 'checked') ? '1' : '0'
			);
		}
	});
	jQuery('#dummy_data_js_export ' + 'button').click(function(event) {
		event.preventDefault();
		window.open('data:text/csv;charset=utf-8,' + escape(exportJSON));
	});
});

function dummy_data_insert_post(num_posts, leaf_only, random_author) {
	if (dummy_data_categories == undefined || dummy_data_categories.length == 0) {
		jQuery('#dummy_data_ajax_response').html('סיום.');
		jQuery('form[name=dummy_data_form]').submit();
		return;
	}
	var cat_id = dummy_data_categories.splice(0, dummy_data_num).join(',');
	//jQuery('#dummy_data_ajax_response').html('Processing category ' + (dummy_data_category_count - dummy_data_categories.length) + ' of ' + dummy_data_category_count);

	jQuery.post(ajaxurl, {
		action: 'dummy_data_ajax',
		security: dummy_data_ajax_nonce,
		num_posts: num_posts,
		leaf_only: leaf_only,
		random_author: random_author,
		cat_id: cat_id
	}, (function(num_posts, leaf_only, random_author) {
			return (function(response) {
				console.log(response);
				if (response === '') {
					dummy_data_insert_post(num_posts, leaf_only, random_author);
				} else {
					jQuery('#dummy_data_ajax_response').html('Error: ' + response);
				}
			});
		})(num_posts, leaf_only, random_author)
	);
}
