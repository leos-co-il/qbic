<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
$main_img = (isset($fields['h_main_img']) && isset($fields['h_main_img']['url'])) ? $fields['h_main_img']['url'] :
		(has_post_thumbnail() ? postThumb() : '');
?>

<section class="home-main">
	<div class="home-main-block">
		<div class="home-content-block">
			<?php if ($fields['h_main_text']) : ?>
				<div class="home-main-text">
					<?= $fields['h_main_text']; ?>
				</div>
			<?php endif;
			getForm('7'); ?>
		</div>
		<?php if ($fields['h_main_img'] || has_post_thumbnail()) : ?>
			<div class="home-image" style="background-image: url('<?= $main_img; ?>')"
		<?php endif; ?>
	</div>
</section>
<?php if ($fields['h_cats_links']) : ?>
	<section class="home-cats">
		<div class="home-cats-wrap">
			<?php foreach ($fields['h_cats_links'] as $link) : ?>
				<div class="col-xl col-lg-4 mb-xl-0 mb-2 home-cat-col">
					<a class="home-cat-item" href="<?= get_term_link($link); ?>">
						<?= $link->name; ?>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif; ?>
<?php if ($fields['h_projects']) : ?>
	<section class="projects-block">
		<div class="projects-grid">
			<?php foreach ($fields['h_projects'] as $x => $project) : ?>
				<div class="project-column project-column-<?= $x + 1; ?>">
					<?php if ($fields['h_projects_title'] && ($x == 2)) : ?>
						<div class="h-projects-title">
							<?= $fields['h_projects_title']; ?>
						</div>
					<?php endif; ?>
					<div class="project-item-inside" <?php if (has_post_thumbnail($project)) : ?>
						style="background-image: url('<?= postThumb($project); ?>')"
					<?php endif; ?>>
						<a class="overlay-link-arrow" href="<?= get_the_permalink($project); ?>"></a>
						<div class="project-overlay">
							<a href="<?= get_the_permalink($project); ?>" class="project-overlay-link">
								<?= $project->post_title; ?>
							</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if ($fields['h_projects_links']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $fields['h_projects_links']['url'];?>" class="base-link white-link">
							<?= (isset($fields['h_projects_links']['title']) && $fields['h_projects_links']['title'])
									? $fields['h_projects_links']['title'] : 'לכל הפרוייקטים שלנו';
							?>
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/repeat', 'form_base');
if ($fields['h_products']) {
	get_template_part('views/partials/content', 'products',
			[
					'products' => $fields['h_products'] ? $fields['h_products'] : '',
					'title' => $fields['h_products_title'] ? $fields['h_products_title'] : 'מוצרים דומים ',
					'link' => $fields['h_products_links'],
			]);
}
if ($fields['h_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['h_slider_seo'],
				'img' => $fields['h_slider_img'],
		]); ?>
	</div>
<?php endif;
if ($fields['h_posts']) : ?>
	<div class="home-posts">
		<?php get_template_part('views/partials/content', 'posts',
				[
						'posts' => $fields['h_posts'],
						'title' => $fields['h_posts_title'] ? $fields['h_posts_title'] : 'קראו איתנו',
						'link' => $fields['h_posts_links']
				]); ?>
	</div>
<?php endif; ?>

<div class="container">
    <div class="row">
        <div class="col-12">
        </div>
    </div>
</div>


<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
