<?php
/*
Template Name: פרויקטים
*/

get_header();
$fields = get_fields();
if (!$fields['choose_projects']) {
	$projects = get_posts([
		'numberposts' => -1,
		'post_type' => 'project',
	]);
} else {
	$projects = $fields['choose_projects'];
}
?>

<?php if ($projects) : ?>
	<section class="projects-page-output page-body article-page-body">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col">
					<h1 class="page-title">
						<?php the_title(); ?>
					</h1>
					<div class="base-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="projects-grid">
			<?php foreach ($projects as $project) : ?>
				<div class="project-item">
					<div class="project-item-inside" <?php if (has_post_thumbnail($project)) : ?>
						style="background-image: url('<?= postThumb($project); ?>')"
					<?php endif; ?>>
						<a class="overlay-link-arrow" href="<?= get_the_permalink($project); ?>"></a>
						<div class="project-overlay">
							<a href="<?= get_the_permalink($project); ?>" class="project-overlay-link">
								<?= $project->post_title; ?>
							</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/repeat', 'form_base');
if ($fields['prod_more_products']) {
	get_template_part('views/partials/content', 'products',
		[
			'products' => $fields['prod_more_products'] ? $fields['prod_more_products'] : '',
			'title' => $fields['prod_more_title'] ? $fields['prod_more_title'] : 'מוצרים דומים ',
			'link' => $fields['prod_more_link']
		]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
