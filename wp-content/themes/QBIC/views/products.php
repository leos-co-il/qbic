<?php
/*
Template Name: מוצרים
*/

get_header();
$fields = get_fields();
$query_args = [
	'post_type' => 'product',
	'posts_per_page' => -1,
];
$posts = new WP_Query($query_args);

?>
<article class="page-body pb-4">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="page-title">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<?php get_template_part('views/partials/repeat', 'sidebar'); ?>
			<div class="col-lg-9 col-md-8 col-sm-7 col-12">
				<?php if ($posts->have_posts()) : ?>
					<div class="row">
						<div class="col-xl-3 col-lg-4 col-sm-6 col-auto">
							<form method="post" id="order">
								<select name="orderby" class="select-sort">
									<option disabled selected>מיין לפי </option>
									<option value="date" name="newest">למיין לפי המעודכן ביותר</option>
									<option value="title" name="title">מיין לפי שם</option>
									<option value="price" name="price">למיין מהזול ליקר</option>
									<option value="price-desc" name="price-desc">למיין מהיקר לזול</option>
								</select>
							</form>
						</div>
					</div>
					<div class="row align-items-stretch put-here-posts justify-content-center put-here-products">
						<?php foreach ($posts->posts as $post) {
							get_template_part('views/partials/card', 'product',
								[
									'post' => $post,
								]);
						} ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form_base');
$samePosts = get_posts([
		'numberposts' => 4,
		'orderby' => 'rand',
		'post_type' => 'product',
]);
if ($fields['prod_more_products'] || $samePosts) {
	get_template_part('views/partials/content', 'products',
			[
					'products' => $fields['prod_more_products'] ? $fields['prod_more_products'] : $samePosts,
					'title' => $fields['prod_more_title'] ? $fields['prod_more_title'] : 'מוצרים דומים ',
			]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

