<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
?>
<article class="page-body contact-page-body">
	<div class="contact-page-back">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h1 class="page-title text-center mb-3"><?php the_title(); ?></h1>
					<div class="base-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="contact-wrapper">
					<div class="row justify-content-center align-items-stretch">
						<div class="col-lg-6 col-md-10 col-11 contact-col">
							<?php if ($tel) : ?>
								<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png">
									</div>
									<div class="contact-info">
										<h3 class="base-title">דברו איתנו</h3>
										<span class="contact-type"><?= $tel; ?></span>
									</div>
								</a>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-mail.png">
									</div>
									<div class="contact-info">
										<h3 class="base-title">כיתבו לנו</h3>
										<span class="contact-type"><?= $mail; ?></span>
									</div>
								</a>
							<?php endif;
							if ($open_hours || $address) : ?>
								<div  class="contact-item wow flipInX" data-wow-delay="0.6s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-hours.png">
									</div>
									<div class="contact-info">
										<h3 class="base-title">בקרו אותנו</h3>
										<div class="d-flex flex-wrap align-items-center justify-content-start">
											<span class="contact-type"><?= $open_hours.' | '; ?></span>
											<a href="https://waze.com/ul?q=<?= $address; ?>">
												<span class="contact-type"><?= $address; ?></span>
											</a>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<div class="col-lg-6 col-12">
							<div class="contact-form-block dark-submit-wrap">
								<?php if ($fields['contact_form_title']) : ?>
									<h2 class="base-title mb-2"><?= $fields['contact_form_title']; ?></h2>
								<?php endif;
								getForm('10'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
