<?php if (isset($args['posts']) && $args['posts']) : ?>
<section class="posts-output">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h2 class="page-title">
					<?= isset($args['title']) && $args['title'] ? $args['title'] : 'מאמרים נוספים'; ?>
				</h2>
			</div>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php foreach ($args['posts'] as $i => $post) : $link = get_the_permalink($post); ?>
				<div class="col-lg-4 col-12 col-post">
					<div class="post-card more-card" data-id="<?= $post->ID; ?>">
						<a class="post-card-image" href="<?= $link; ?>"
							<?php if (has_post_thumbnail($post)) : ?>
								style="background-image: url('<?= postThumb($post); ?>')"
							<?php endif;?>>
						</a>
						<div class="post-card-content">
							<a class="post-card-title" href="<?= $link; ?>"><?= $post->post_title; ?></a>
							<p class="base-text mb-3">
								<?= text_preview($post->post_content, 20); ?>
							</p>
						</div>
						<a href="<?= $link; ?>" class="base-link post-card-link">
							<?= esc_html__('קרא עוד ', 'leos'); ?>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if (isset($args['link']) && $args['link']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $args['link']['url'];?>" class="base-link black-link">
							<?= (isset($args['link']['title']) && $args['link']['title'])
									? $args['link']['title'] : 'לכל המאמרים';
							?>
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php endif; ?>
