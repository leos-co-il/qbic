<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-4 col-lg-6 col-12 col-post">
		<div class="post-card product-card more-card">
			<a class="post-card-image product-card-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-card-content">
				<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="base-text mb-3">
					<?= text_preview($args['post']->post_content, 20); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="base-link post-card-link">
				<?= esc_html__('מעבר למוצר', 'leos'); ?>
			</a>
		</div>
	</div>
<?php endif; ?>
