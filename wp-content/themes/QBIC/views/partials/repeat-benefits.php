<?php if ($benefits = opt('benefit_item')) : ?>
	<section class="benefits-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-md-10 col-11">
					<?php if ($ben_title = opt('benefits_block_title')) : ?>
						<h2 class="benefits-block-title"><?= $ben_title; ?></h2>
					<?php endif; ?>
					<div class="benefits-output">
						<?php foreach ($benefits as $y => $benefit) : ?>
							<div class="benefit-item">
								<div class="benefit-content">
									<?php if ($benefit['ben_title']) : ?>
										<h3 class="base-title">
											<?= $benefit['ben_title']; ?>
										</h3>
									<?php endif; ?>
									<p class="base-text benefit-text">
										<?= $benefit['ben_text']; ?>
									</p>
								</div>
								<div class="benefit-icon">
									<?php if ($benefit['ben_icon']) : ?>
										<img src="<?= $benefit['ben_icon']['url']; ?>" alt="benefit">
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
