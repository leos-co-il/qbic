<section class="repeat-form-inline dark-submit-wrap">
	<div class="container">
		<div class="row justify-content-center form-wrapper">
			<div class="col-12">
				<div class="row align-items-center mb-2">
					<?php if ($title = opt('post_form_title')) : ?>
						<div class="col-auto">
							<h2 class="form-title"><?= $title; ?></h2>
						</div>
					<?php endif;
					if ($subtitle = opt('post_form_subtitle')) : ?>
						<div class="col-auto">
							<h3 class="base-form-subtitle mb-0"><?= $subtitle; ?></h3>
						</div>
					<?php endif; ?>
				</div>
				<?php getForm('22'); ?>
			</div>
		</div>
	</div>
</section>
