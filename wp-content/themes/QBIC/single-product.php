<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$post_terms = wp_get_object_terms($postId, 'product_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'product',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'product',
		'post__not_in' => array($postId),
	]);
}
$post_gallery = $fields['gallery'];
?>
<article class="article-page-body page-body pb-5">
	<div class="container">
		<div class="row col-title-mobile">
			<div class="col-12">
				<h1 class="base-title mb-3"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row justify-content-between">
			<div class="col-xl-4 col-lg-5 col-12 d-flex flex-column align-items-start">
				<h1 class="base-title base-title-desktop mb-3"><?php the_title(); ?></h1>
				<?php if ($fields['price'] || $fields['price_sale']) : ?>
					<div class="price-wrapper">
						<?php if ($fields['price']) : ?>
							<div class="middle-title">
								<?= '₪'.$fields['price']; ?>
							</div>
						<?php endif;
						if ($fields['price_sale']) : ?>
							<div class="new-price">
								<?= '₪'.$fields['price_sale']; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<div class="base-output post-text-output">
					<?php the_content(); ?>
				</div>
				<div>
					<?php if ($fields['files_post']) : ?>
						<div class="share-items-line">
							<?php foreach ($fields['files_post'] as $file) : ?>
								<a class="share-item-file" download href="<?= $file['file']['url']; ?>">
									<span class="share-item">
										<img src="<?= ICONS ?>file.png" alt="download-file">
									</span>
									<span class="base-text social-item-text">
										<?= (isset($file['file']['title']) && $file['file']['title']) ? $file['file']['title'] : 'הורידו את הקובץ'?>
									</span>
								</a>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<div class="share-items-line">
						<a class="share-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>mail.png" alt="share-by-email">
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="share-item">
							<img src="<?= ICONS ?>whatsapp.png" alt="share-by-whatsapp">
						</a>
						<span class="base-text social-item-text">
							שתף את המוצר
						</span>
					</div>
				</div>
			</div>
			<?php if ($post_gallery || has_post_thumbnail()) : ?>
				<div class="col-xl-8 col-lg-7 col-12 product-gallery-col">
					<div class="arrows-slider post-gallery-block">
						<div class="gallery-slider" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= postThumb(); ?>')"
									   href="<?= postThumb(); ?>" data-lightbox="images"></a>
								</div>
							<?php endif;
							if ($post_gallery) : foreach ($post_gallery as $img): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= $img['url']; ?>')"
									   href="<?= $img['url']; ?>" data-lightbox="images">
									</a>
								</div>
							<?php endforeach; endif; ?>
						</div>
						<?php if ($post_gallery) : ?>
						<div class="thumbs-wrap">
							<div class="thumbs" dir="rtl">
								<?php if(has_post_thumbnail()): ?>
									<div class="p-1">
										<a class="thumb-item" style="background-image: url('<?= postThumb(); ?>')"
										   href="<?= postThumb(); ?>" data-lightbox="images-small"></a>
									</div>
								<?php endif; foreach ($post_gallery as $img): ?>
									<div class="p-1">
										<a class="thumb-item" style="background-image: url('<?= $img['url']; ?>')"
										   href="<?= $img['url']; ?>" data-lightbox="images-small">
										</a>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="put-arrows-here"></div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'form_inline');
if ($samePosts) {
	get_template_part('views/partials/content', 'products',
		[
			'products' => $samePosts,
			'title' => $fields['same_title'] ? $fields['same_title'] : 'מוצרים דומים ',
		]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $fields['single_slider_seo'],
		]);
}
get_footer(); ?>
