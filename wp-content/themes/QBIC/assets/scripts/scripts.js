(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.sidebar-item-trigger').click(function () {
			$(this).toggleClass('rotated');
			$(this).parent('.sidebar-item-inside').parent('.sidebar-item').children('.children-terms-wrap').slideFadeToggle();
		});
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$('.search-trigger').click(function () {
			$('.search-form').addClass('show-popup');
			$('.float-search-form').addClass('show-float-form');
		});
		$('.close-search').click(function () {
			$('.search-form').removeClass('show-popup');
			$('.float-search-form').removeClass('show-float-form');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form-pop').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		var thumbsSlider = $('.thumbs');
		thumbsSlider.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.gallery-slider',
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1500,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		var prevAr = thumbsSlider.children('.slick-prev');
		var nextAr = thumbsSlider.children('.slick-next');
		$('.put-arrows-here').append(prevAr).append(nextAr);
		$('.post-gallery-slider').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			centerMode: true,
			rtl: true,
			arrows: true,
			dots: false,
			centerPadding: '150px',
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 4,
						centerPadding: '100px',
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
						centerPadding: '100px',
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
						centerPadding: '50px',
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						centerPadding: '50px',
					}
				},
				// {
				// 	breakpoint: 500,
				// 	settings: {
				// 		slidesToShow: 1,
				// 	}
				// },
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-post').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').addClass('active-faq');
			show.parent().children('.question-title').children('.arrow-top').addClass('hide');
			show.parent().children('.question-title').children('.arrow-bottom').addClass('show');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').removeClass('active-faq');
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('hide');
			collapsed.parent().children('.question-title').children('.arrow-bottom').removeClass('show');
		});
		var prodAcc = $('#accordion-product');
		prodAcc.on('shown.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().addClass('active');
		});

		prodAcc.on('hide.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().removeClass('active');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});

	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
		var postType = $(this).data('type');
		var termID = $(this).data('term');
		var termName = $(this).data('term_name');
		// var params = $('.take-json').html();
		var ids = '';
		var page = $(this).data('page');
		var countAll = $(this).data('count');
		var quantity = $('.more-card').length;
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				termName: termName,
				ids: ids,
				page: page,
				quantity: quantity,
				countAll: countAll,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
	var termID = $('#order').data('id');
	console.log(termID);
	$('#order').on('change', function(e) {
		e.preventDefault();
		var orderType = $(this).find(':selected').val();
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				orderType: orderType,
				termID: termID,
				action: 'order_function',
			},
			success: function (data) {
				$('.put-here-products').html(data.html);
			}
		});
	});

})( jQuery );
