<?php
$query = get_queried_object();
get_header();
$posts = new WP_Query([
	'posts_per_page' => 9,
	'post_type' => 'post',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'post',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="page-title">
					<?= $query->name; ?>
				</h1>
				<div class="base-output text-center">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 9)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="base-link more-link load-more-posts" data-type="post" data-count="<?= $num; ?>"
						 data-term_name="category" data-term="<?= $query->term_id;?>">
						<?= esc_html__('טען עוד', 'leos'); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
	]);
}
get_footer(); ?>

