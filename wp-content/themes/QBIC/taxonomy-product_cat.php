<?php

get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'product',
	'tax_query' => array(
		array(
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
?>
<article class="page-body pb-4">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="page-title">
					<?= $query->name; ?>
				</h1>
				<div class="base-output text-center">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<?php get_template_part('views/partials/repeat', 'sidebar'); ?>
			<div class="col-lg-9">
				<?php if ($posts->have_posts()) : ?>
					<div class="row">
						<div class="col-xl-3 col-lg-4 col-sm-6 col-auto">
							<form method="post">
								<select name="orderby" class="select-sort" id="order" data-id="<?= $query->term_id; ?>">
									<option disabled selected>מיין לפי </option>
									<option value="date" name="newest">למיין לפי המעודכן ביותר</option>
									<option value="title" name="title">מיין לפי שם</option>
									<option value="price" name="price">למיין מהזול ליקר</option>
									<option value="price-desc" name="price-desc">למיין מהיקר לזול</option>
								</select>
							</form>
						</div>
					</div>
					<div class="row align-items-stretch put-here-products justify-content-center">
						<?php foreach ($posts->posts as $post) {
							get_template_part('views/partials/card', 'product',
								[
									'post' => $post,
								]);
						} ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form_base');
$samePosts = get_posts([
	'numberposts' => 4,
	'orderby' => 'rand',
	'post_type' => 'product',
]);
if ($samePosts) {
	get_template_part('views/partials/content', 'products',
		[
			'products' => $samePosts,
			'title' => 'מוצרים דומים',
		]);
}
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
	]);
}
get_footer(); ?>
