<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header class="<?= is_front_page() ? 'home-header' : ''; ?>">
	<div class="drop-menu">
		<nav id="MainNav" class="h-100">
			<div id="MobNavBtn">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
		</nav>
	</div>
	<div class="container-fluid">
        <div class="row align-items-center">
			<div class="col-lg-3 col-auto header-socials">
				<div class="head-link-wrap menu-trigger">
					<button class="hamburger hamburger--spin" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<?php if ($instagram = opt('instagram')) : ?>
					<a class="head-link-wrap" href="<?= $instagram; ?>">
						<img src="<?= ICONS ?>instagram.png" alt="instagram">
					</a>
				<?php endif;
				if ($facebook = opt('facebook')) : ?>
					<a class="head-link-wrap" href="<?= $facebook; ?>">
						<img src="<?= ICONS ?>facebook.png" alt="facebook">
					</a>
				<?php endif; ?>
				<div class="head-link-wrap search-trigger">
					<img src="<?= ICONS ?>search.png" alt="search">
				</div>
			</div>
			<div class="col d-flex align-items-center">
				<?php if ($address = opt('address')) : ?>
					<a href="https://waze.com/ul?q=<?= $address; ?>" class="header-address">
						<?= 'בקרו אותנו - '.$address; ?>
					</a>
				<?php endif;
				if ($tel = opt('tel')) : ?>
					<a href="tel:<?= $tel; ?>" class="header-tel">
						<img src="<?= ICONS ?>header-tel.png" alt="call-us">
						<span><?= $tel; ?></span>
					</a>
				<?php endif; ?>
				<div class="head-link-wrap pop-trigger">
					<img src="<?= ICONS ?>mail.png" alt="contact-us">
				</div>
			</div>
			<?php if ($logo = opt('logo')) : ?>
				<div class="col-auto">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				</div>
			<?php endif; ?>
        </div>
    </div>
</header>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-9 col-sm-10 col-12 d-flex justify-content-center">
				<div class="float-form d-flex flex-column align-items-center">
					<div class="form-wrapper-pop">
						<span class="close-form close-form-pop">
							<?= svg_simple(ICONS.'close.svg'); ?>
						</span>
						<?php if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="form-title pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						getForm('6'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="search-form">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-9 col-sm-10 col-12 d-flex justify-content-center">
				<div class="float-search-form d-flex flex-column align-items-center">
					<div class="form-wrapper-pop">
						<span class="close-form close-search">
							<?= svg_simple(ICONS.'close.svg'); ?>
						</span>
						<?php if ($f_title = opt('search_form_title')) : ?>
							<h2 class="form-title pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
