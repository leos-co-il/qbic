<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$current_id = get_queried_object_id();
$contact_id = getPageByTemplate('views/contact.php');
$logo = opt('logo');
$id = get_the_ID();
$locations = get_nav_menu_locations();
$menu_obj_main = wp_get_nav_menu_object($locations['footer-menu']);
$menu_obj_links = wp_get_nav_menu_object($locations['footer-links-menu']);
$main_menu_title = get_field('menu_title', $menu_obj_main);
$link_menu_title = get_field('menu_title', $menu_obj_links);
?>

<footer>
	<div>
		<?php if ($id !== getPageByTemplate('views/contact.php') && $id !== getPageByTemplate('views/contact.php')) : ?>
			<div class="footer-form">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="row justify-content-center align-items-end">
								<?php if ($logo = opt('logo')) : ?>
									<div class="col-sm-auto col-7 mb-3">
										<a href="/" class="logo p-0">
											<img src="<?= $logo['url'] ?>" alt="logo">
										</a>
									</div>
								<?php endif; ?>
								<div class="col-sm col-12 mb-3 col-form-titles">
									<?php if ($title = opt('foo_form_title')) : ?>
										<span class="form-title foo-form-title"><?= $title; ?></span>
									<?php endif;
									if ($subtitle = opt('foo_form_subtitle')) : ?>
										<span class="form-title foo-form-subtitle"><?= $subtitle; ?></span>
									<?php endif; ?>
								</div>
								<div class="col-12">
									<?php getForm('9'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="footer-main">
			<a id="go-top">
				<span class="go-top-arrow"></span>
				<h5 class="to-top-text">
					למעלה
				</h5>
			</a>
			<div class="container footer-container-menu">
				<div class="row justify-content-center">
					<div class="col-md-3 col-6 foo-menu">
						<h3 class="foo-title"><?= $main_menu_title; ?></h3>
						<div class="menu-border-top">
							<?php getMenu('footer-menu', '1'); ?>
						</div>
					</div>
					<div class="col-md col-12 foo-menu foo-menu-links">
						<h3 class="foo-title"><?= $link_menu_title; ?></h3>
						<div class="menu-border-top">
							<?php getMenu('footer-links-menu', '1', 'two-columns'); ?>
						</div>
					</div>
					<div class="col-md-3 col-6 foo-contact-menu">
						<h3 class="foo-title">צור קשר</h3>
						<div class="menu-border-top">
							<ul class="contact-list">
								<?php  if ($address) : ?>
									<li>
										<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="foo-link">
											<?= $address; ?>
										</a>
									</li>
								<?php endif;
								if ($tel) : ?>
									<li>
										<a href="tel:<?= $tel; ?>" class="foo-link">
											<?= $tel; ?>
										</a>
									</li>
								<?php endif;
								if ($fax) : ?>
									<li>
										<div class="foo-link fax-item">
											<?= $fax; ?>
										</div>
									</li>
								<?php endif;
								if ($mail) : ?>
									<li>
										<a href="mailto:<?= $mail; ?>" class="foo-link">
											<?= $mail; ?>
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
						<div class="foo-socials-line">
							<?php if ($instagram = opt('instagram')) : ?>
								<a href="<?= $instagram; ?>" class="foo-social-link-item">
									<i class="fab fa-instagram"></i>
								</a>
							<?php endif;
							if ($facebook = opt('facebook')) : ?>
								<a href="<?= $facebook; ?>" class="foo-social-link-item">
									<i class="fab fa-facebook-f"></i>
								</a>
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
