<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$currentType = get_post_type($postId);
$taxname = 'category';
switch ($currentType) {
	case 'project':
		$taxname = 'project_cat';
		$sameTitle = '<h2>פרויקטים נוספים</h2>';
		break;
	case 'post':
		$taxname = 'category';
		$sameTitle = '<h2>מאמרים נוספים</h2>';
		break;
	default:
		$taxname = 'category';
		$sameTitle = '<h2>מאמרים נוספים</h2>';
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 3,
	'post_type' => $currentType,
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => $taxname,
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'post_type' => $currentType,
		'post__not_in' => array($postId),
	]);
}
$post_gallery = $fields['gallery'];
?>
<article class="article-page-body page-body pb-3">
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-xl-10 col-lg-11 col-12 d-flex flex-column align-items-start">
				<div class="base-output post-text-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<div>
					<?php if ($fields['files_post']) : ?>
						<div class="share-items-line">
							<?php foreach ($fields['files_post'] as $file) : ?>
								<a class="share-item-file" download href="<?= $file['file']['url']; ?>">
									<span class="share-item">
										<img src="<?= ICONS ?>file.png" alt="download-file">
									</span>
									<span class="base-text social-item-text">
										<?= (isset($file['file']['title']) && $file['file']['title']) ? $file['file']['title'] : 'הורידו את הקובץ'?>
									</span>
								</a>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<div class="share-items-line">
						<a class="share-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>mail.png" alt="share-by-email">
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="share-item">
							<img src="<?= ICONS ?>whatsapp.png" alt="share-by-whatsapp">
						</a>
						<span class="base-text social-item-text">
							שתף את המאמר
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($post_gallery || has_post_thumbnail()) : ?>
		<div class="arrows-slider post-gallery-block">
			<div class="post-gallery-slider" dir="rtl">
				<?php if (has_post_thumbnail()) : ?>
					<div class="gallery-slider-wrapper">
						<div class="gallery-item" style="background-image: url('<?= postThumb(); ?>')">
							<a class="gallery-overlay" data-lightbox="images" href="<?= postThumb(); ?>"></a>
						</div>
					</div>
				<?php endif;
				if ($post_gallery) : foreach ($post_gallery as $img) : ?>
					<div class="gallery-slider-wrapper">
						<div class="gallery-item" style="background-image: url('<?= $img['url']; ?>')">
							<a class="gallery-overlay" data-lightbox="images" href="<?= $img['url']; ?>"></a>
						</div>
					</div>
				<?php endforeach; endif;?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'form_inline');
if ($samePosts) {
	get_template_part('views/partials/content', 'posts',
		[
			'posts' => $samePosts,
			'title' => $fields['same_title'] ? $fields['same_title'] : $sameTitle,
		]);
}
if ($fields['single_slider_seo']) {
		get_template_part('views/partials/content', 'slider',
			[
				'img' => $fields['slider_img'],
				'content' => $fields['single_slider_seo'],
			]);
}
get_footer(); ?>
